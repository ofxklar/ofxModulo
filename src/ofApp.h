#pragma once

#include "ofMain.h"

#include "ofxGui.h"
#include "ofxOscParameterSync.h"

class ofApp : public ofBaseApp{

 public:
  void setup();
  void update();
  void draw();

  void exit();

  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseEntered(int x, int y);
  void mouseExited(int x, int y);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);

  void getPoints();

  void xChanged(float & x);
  void moduloChanged(float & modulo);

  // GUI

  ofxOscParameterSync sync;

  ofxPanel gui;

  ofParameter<bool> draw_gui;
  ofxFloatSlider x;
  ofxFloatSlider modulo;
  ofxFloatSlider radius;
  ofxFloatSlider speed_x;
  ofxIntSlider fps;

  // modulo
  vector<ofPoint> points_in;
  vector<ofPoint> points_out;
  ofPoint center;
};
