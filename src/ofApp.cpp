#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

  // GUI
  gui.setup("ofxModulo"); // most of the time you don't need a name

  gui.add(draw_gui.set("draw_gui", true));
  gui.add(x.setup("x", 0.0, 0.1, 1000.0));
  gui.add(speed_x.setup("speed x", 1.0, -10.0, 10.0));
  gui.add(modulo.setup("modulo", 500.0, 1.0, 1000.0));
  gui.add(fps.setup("fps", 0, 0, 100));

  x.addListener(this, &ofApp::moduloChanged);
  modulo.addListener(this, &ofApp::moduloChanged);

  sync.setup((ofParameterGroup&)gui.getParameter(),6669,"localhost",6668);

  radius = 500;

  center.set(ofGetScreenWidth()/2.0, ofGetScreenHeight()/2.0);

  for (int i = 0; i < 1000; ++i)
    {
      points_in.push_back(ofPoint(0,0));
      points_out.push_back(ofPoint(0,0));
    }

  getPoints();
}

void ofApp::getPoints(){
  float angle = 2*PI/modulo;
  for (int i = 0; i < modulo; ++i)
    {
      points_in[i] = ofPoint(center.x + radius * cos(angle*i),
			     center.y + radius * sin(angle*i));
      points_out[i] = ofPoint(center.x + radius * cos(angle*i*x),
			      center.y + radius * sin(angle*i*x));
    }
}

//--------------------------------------------------------------
void ofApp::update(){
  sync.update();
  fps = ofGetFrameRate();

  float correction = ofMap(ofGetLastFrameTime(), 0, 0.02, 0, 1);

  x = x + correction*speed_x/1000;

  cout << correction << endl;

}

void ofApp::xChanged(float & x){

}

void ofApp::moduloChanged(float & modulo){
  getPoints();
}

//--------------------------------------------------------------
void ofApp::draw(){
  for (int i = 0; i < modulo; ++i)
    {
      ofDrawLine(points_in[i], points_out[i]);
    }

  if (draw_gui){
    gui.draw();
  }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
    break;
  case 'g':
    draw_gui = !draw_gui;
    break;
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void ofApp::exit(){
  x.removeListener(this, &ofApp::moduloChanged);
  modulo.removeListener(this, &ofApp::moduloChanged);
}
